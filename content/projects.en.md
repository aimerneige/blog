---
title: "Projects 💻"
layout: "projects"
url: "/en/projects"
summary: "projects"
---

This page showcases a list of open source projects developed or participated by me over the period of time.

### [DDLC-Plus-Asset-Decrypter](https://github.com/aimerneige/DDLC-Plus-Asset-Decrypter)

「心跳文学部 Plus」资源文件解密器。

### [MiraiChess](https://github.com/aimerneige/MiraiChess)

使用 MiraiGo-Template 实现的国际象棋机器人。可以在群内下棋，同时支持 elo 等级分计算。

### [yukichan-bot-module](https://github.com/yukichan-bot-module)

一系列 MiraiGo-Template 插件。

### [blog](https://github.com/aimerneige/blog)

技术博客，就是你现在正在访问的站点。

### [GoWebHooks](https://github.com/fzf404/GoWebHooks)

Golang 实现的 WebHooks 部署脚本。简单且轻量化地实现自动化部署。

### [i3status](https://github.com/aimerneige/i3status)

i3status 配置文件。

### [markdowntutorial.com](https://github.com/gjtorikian/markdowntutorial.com)

一个学习 markdown 的网站。贡献了部分中文翻译。

### [Music-Player-GO](https://github.com/enricocid/Music-Player-GO)

一个音乐播放器。贡献了部分中文翻译。

### [hugo-theme-heimu](https://github.com/aimerneige/hugo-theme-heimu)

在 hugo 内使用黑幕。{{< heimu >}}就是这个样子{{< /heimu >}}

### [muzei_coolapk](https://github.com/mzdluo123/muzei_coolapk)

酷安酷图的 [muzei](https://muzei.co/) 壁纸源插件。图片质量还是可以的。

### [SurfingTutorial](https://github.com/mzdluo123/SurfingTutorial)

给中国新大学生的网上冲浪和计算机使用教程。

### [GlowSans-Ubuntu-Magisk](https://github.com/aimerneige/GlowSans-Ubuntu-Magisk)

自己用的 Magisk 字体模块。

### [EdibleFlowers](https://github.com/OptimistQAQ/EdibleFlowers)

之前的一个比赛项目，没什么好说的。

### [CourseEvaluation](https://github.com/aimerneige/CourseEvaluation)

大学评课系统，JavaEE/数据库 课程设计。

### [CourseEvaluationClient](https://github.com/aimerneige/CourseEvaluationClient)

大学评课系统客户端，JavaEE/数据库 课程设计。

### [RFIDAndroid](https://github.com/aimerneige/RFIDAndroid)

rfid 课程设计移动端代码。主要是蓝牙串口通讯。

### [boxes](https://github.com/aimerneige/boxes)

推箱子，程序设计基础课程设计。

### [records-of-Jing](https://github.com/holk-h/records-of-Jing)

这个好像不能解释太多（逃

### [jiwang](https://github.com/aimerneige/jiwang)

计网复习资料，祝大家都不挂科。

### [mayuan](https://github.com/aimerneige/mayuan)

马原复习小工具，祝大家都不挂科。

### [web-paste-archive](https://github.com/aimerneige/web-paste-archive)

web paste 服务，因为前端不太会写咕了。

### [FuckHomework](https://github.com/aimerneige/FuckHomework)

手写作业生成器，这个项目应该有很多人写了。

### [auto-mail](https://github.com/aimerneige/auto-mail)

一个自动发送邮件的小工具。

### [you-get-helper](https://github.com/aimerneige/you-get-helper)

用 you-get 下载某 up 的全部视频。

### [DuiTang](https://github.com/aimerneige/DuiTang)

堆糖爬虫，已经不能用了。为什么这种东西可以有 star？

### [comic2pdf](https://github.com/aimerneige/comic2pdf)

漫画图片转 pdf。{{< heimu >}}本子转 pdf。{{< /heimu >}}

### [Script](https://github.com/aimerneige/Script)

自用的一些脚本。

### [HeicWallpapers](https://github.com/aimerneige/HeicWallpapers)

kde 下的 Heic 动态壁纸。

### [HelloVala](https://github.com/aimerneige/HelloVala)

请不要学习 vala。
